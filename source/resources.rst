.. _resources:

Resources for Debian Members
********************************************************************************************************************************

In this chapter you will find a very brief roadmap of the Debian mailing
lists, the Debian machines which may be available to you as a member,
and all the other resources that are available to help you in your work.

.. _mailing-lists:

Mailing lists
================================================================================================================================

Much of the conversation between Debian developers (and users) is
managed through a wide array of mailing lists we host at
``lists.debian.org``. To find out more on how to subscribe or
unsubscribe, how to post and how not to post, where to find old posts
and how to search them, how to contact the list maintainers and see
various other information about the mailing lists, please read
https://www.debian.org/MailingLists/\ . This section will only
cover aspects of mailing lists that are of particular interest to
developers.

.. _mailing-lists-rules:

Basic rules for use
--------------------------------------------------------------------------------------------------------------------------------

When replying to messages on the mailing list, please do not send a
carbon copy (``CC``) to the original poster unless they explicitly
request to be copied. Anyone who posts to a mailing list should read it
to see the responses.

Cross-posting (sending the same message to multiple lists) is
discouraged. As ever on the net, please trim down the quoting of
articles you're replying to. In general, please adhere to the usual
conventions for posting messages.

Please read the `code of
conduct <https://www.debian.org/MailingLists/#codeofconduct>`__ for more
information. The `Debian Community
Guidelines <https://people.debian.org/~enrico/dcg/>`__ are also worth
reading.

.. _core-devel-mailing-lists:

Core development mailing lists
--------------------------------------------------------------------------------------------------------------------------------

The core Debian mailing lists that developers should use are:

-  ``debian-devel-announce@lists.debian.org``, used to announce
   important things to developers. All developers are expected to be
   subscribed to this list.

-  ``debian-devel@lists.debian.org``, used to discuss various
   development related technical issues.

-  ``debian-policy@lists.debian.org``, where the Debian Policy is
   discussed and voted on.

-  ``debian-project@lists.debian.org``, used to discuss various
   non-technical issues related to the project.

There are other mailing lists available for a variety of special topics;
see https://lists.debian.org/\  for a list.

.. _mailing-lists-special:

Special lists
--------------------------------------------------------------------------------------------------------------------------------

``debian-private@lists.debian.org`` is a special mailing list for
private discussions amongst Debian developers. It is meant to be used
for posts which for whatever reason should not be published publicly. As
such, it is a low volume list, and users are urged not to use
``debian-private@lists.debian.org`` unless it is really necessary.
Moreover, do *not* forward email from that list to anyone. Archives of
this list are not available on the web for obvious reasons, but you can
see them using your shell account on ``master.debian.org`` and looking
in the ``~debian/archive/debian-private/`` directory.

``debian-email@lists.debian.org`` is a special mailing list used as a
grab-bag for Debian related correspondence such as contacting upstream
authors about licenses, bugs, etc. or discussing the project with others
where it might be useful to have the discussion archived somewhere.

.. _mailing-lists-new:

Requesting new development-related lists
--------------------------------------------------------------------------------------------------------------------------------

Before requesting a mailing list that relates to the development of a
package (or a small group of related packages), please consider if using
an alias (via a .forward-aliasname file on master.debian.org, which
translates into a reasonably nice *you-aliasname@debian.org* address) is
more appropriate.

If you decide that a regular mailing list on lists.debian.org is really
what you want, go ahead and fill in a request, following `the
HOWTO <https://www.debian.org/MailingLists/HOWTO_start_list>`__.

.. _irc-channels:

IRC channels
================================================================================================================================

Several IRC channels are dedicated to Debian's development. They are
mainly hosted on the `Open and free technology community
(OFTC) <https://www.oftc.net/>`__ network. The ``irc.debian.org`` DNS
entry is an alias to ``irc.oftc.net``.

The main channel for Debian in general is ``#debian``. This is a large,
general-purpose channel where users can find recent news in the topic
and served by bots. ``#debian`` is for English speakers; there are also
``#debian.de``, ``#debian-fr``, ``#debian-br`` and other similarly named
channels for speakers of other languages.

The main channel for Debian development is ``#debian-devel``. It is a
very active channel; it will typically have a minimum of 150 people at
any time of day. It's a channel for people who work on Debian, it's not
a support channel (there's ``#debian`` for that). It is however open to
anyone who wants to lurk (and learn). Its topic is commonly full of
interesting information for developers.

Since ``#debian-devel`` is an open channel, you should not speak there
of issues that are discussed in ``debian-private@lists.debian.org``.
There's another channel for this purpose, it's called
``#debian-private`` and it's protected by a key. This key is available
at ``master.debian.org:~debian/misc/irc-password``.

There are other additional channels dedicated to specific subjects.
``#debian-bugs`` is used for coordinating bug squashing parties.
``#debian-boot`` is used to coordinate the work on the debian-installer.
``#debian-doc`` is occasionally used to talk about documentation, like
the document you are reading. Other channels are dedicated to an
architecture or a set of packages: ``#debian-kde``, ``#debian-dpkg``,
``#debian-perl``, ``#debian-python``...

Some non-English developers' channels exist as well, for example
``#debian-devel-fr`` for French speaking people interested in Debian's
development.

Channels dedicated to Debian also exist on other IRC networks, notably
on the `freenode <https://www.freenode.net/>`__ IRC network, which was
pointed at by the ``irc.debian.org`` alias until 4th June 2006.

To get a cloak on freenode, you send Jörg Jaspert <joerg@debian.org> a
signed mail where you tell what your nick is. Put cloak somewhere in the
Subject: header. The nick should be registered: `Nick Setup
Page <https://freenode.net/faq.shtml#nicksetup>`__. The mail needs to be
signed by a key in the Debian keyring. Please see `Freenode
documentation <https://freenode.net/faq.shtml#projectcloak>`__ for more
information about cloaks.

.. _doc-rsrcs:

Documentation
================================================================================================================================

This document contains a lot of information which is useful to Debian
developers, but it cannot contain everything. Most of the other
interesting documents are linked from `The Developers'
Corner <https://www.debian.org/devel/>`__. Take the time to browse all
the links; you will learn many more things.

.. _server-machines:

Debian machines
================================================================================================================================

Debian has several computers working as servers, most of which serve
critical functions in the Debian project. Most of the machines are used
for porting activities, and they all have a permanent connection to the
Internet.

Some of the machines are available for individual developers to use, as
long as the developers follow the rules set forth in the `Debian Machine
Usage Policies <https://www.debian.org/devel/dmup>`__.

Generally speaking, you can use these machines for Debian-related
purposes as you see fit. Please be kind to system administrators, and do
not use up tons and tons of disk space, network bandwidth, or CPU
without first getting the approval of the system administrators. Usually
these machines are run by volunteers.

Please take care to protect your Debian passwords and SSH keys installed
on Debian machines. Avoid login or upload methods which send passwords
over the Internet in the clear, such as Telnet, FTP, POP etc.

Please do not put any material that doesn't relate to Debian on the
Debian servers, unless you have prior permission.

The current list of Debian machines is available at
https://db.debian.org/machines.cgi\ . That web page contains
machine names, contact information, information about who can log in,
SSH keys etc.

If you have a problem with the operation of a Debian server, and you
think that the system operators need to be notified of this problem, you
can check the list of open issues in the DSA (Debian System
Administration) Team's queue of our request tracker at
https://rt.debian.org/\  (you can login with user "debian", its
password is available at
``master.debian.org:~debian/misc/rt-password``). To report a new problem
in the request tracker, simply send a mail to ``admin@rt.debian.org``
and make sure to put the string "Debian RT" somewhere in the subject. To
contact the DSA team by email, use ``dsa@debian.org`` for anything that
contains private or privileged information and should not be made
public, and ``debian-admin@lists.debian.org`` otherwise. The DSA team is
also present on the ``#debian-admin`` IRC channel on OFTC.

If you have a problem with a certain service, not related to the system
administration (such as packages to be removed from the archive,
suggestions for the web site, etc.), generally you'll report a bug
against a *pseudo-package*. See :ref:`submit-bug` for information on
how to submit bugs.

Some of the core servers are restricted, but the information from there
is mirrored to another server.

.. _servers-bugs:

The bugs server
--------------------------------------------------------------------------------------------------------------------------------

``bugs.debian.org`` is the canonical location for the Bug Tracking
System (BTS).

If you plan on doing some statistical analysis or processing of Debian
bugs, this would be the place to do it. Please describe your plans on
``debian-devel@lists.debian.org`` before implementing anything, however,
to reduce unnecessary duplication of effort or wasted processing time.

.. _servers-ftp-master:

The ftp-master server
--------------------------------------------------------------------------------------------------------------------------------

The ``ftp-master.debian.org`` server holds the canonical copy of the
Debian archive. Generally, packages uploaded to ftp.upload.debian.org
end up on this server; see :ref:`upload`.

It is restricted; a mirror is available on
``mirror.ftp-master.debian.org``.

Problems with the Debian FTP archive generally need to be reported as
bugs against the ``ftp.debian.org`` pseudo-package or an email to
``ftpmaster@debian.org``, but also see the procedures in
:ref:`archive-manip`.

.. _servers-www:

The www-master server
--------------------------------------------------------------------------------------------------------------------------------

The main web server is ``www-master.debian.org``. It holds the official
web pages, the face of Debian for most newbies.

If you find a problem with the Debian web server, you should generally
submit a bug against the pseudo-package ``www.debian.org``. Remember to
check whether or not someone else has already reported the problem to
the `Bug Tracking System <https://bugs.debian.org/www.debian.org>`__.

.. _servers-people:

The people web server
--------------------------------------------------------------------------------------------------------------------------------

``people.debian.org`` is the server used for developers' own web pages
about anything related to Debian.

If you have some Debian-specific information which you want to serve on
the web, you can do this by putting material in the ``public_html``
directory under your home directory on ``people.debian.org``. This will
be accessible at the URL
``https://people.debian.org/~``\ *your-user-id*\ ``/``.

You should only use this particular location because it will be backed
up, whereas on other hosts it won't.

Usually the only reason to use a different host is when you need to
publish materials subject to the U.S. export restrictions, in which case
you can use one of the other servers located outside the United States.

Send mail to ``debian-devel@lists.debian.org`` if you have any
questions.

.. _salsa-debian-org:

salsa.debian.org: Git repositories and collaborative development platform
--------------------------------------------------------------------------------------------------------------------------------

If you want to use a git repository for any of your Debian work, you can
use Debian's GitLab instance called `Salsa <https://salsa.debian.org>`__
for that purpose. Gitlab provides also the possibility to have merge
requests, wiki pages, bug trackers among many other services as well as
a fine-grained tuning of access permission, to help working on projects
collaboratively.

For more information, please see the documentation at
https://wiki.debian.org/Salsa/Doc\ .

.. _dchroot:

chroots to different distributions
--------------------------------------------------------------------------------------------------------------------------------

On some machines, there are chroots to different distributions
available. You can use them like this:

::

   vore$ dchroot unstable
   Executing shell in chroot: /org/vore.debian.org/chroots/user/unstable

In all chroots, the normal user home directories are available. You can
find out which chroots are available via
https://db.debian.org/machines.cgi\ .

.. _devel-db:

The Developers Database
================================================================================================================================

The Developers Database, at https://db.debian.org/\ , is an LDAP
directory for managing Debian developer attributes. You can use this
resource to search the list of Debian developers. Part of this
information is also available through the finger service on Debian
servers; try ``finger
yourlogin@db.debian.org`` to see what it reports.

Developers can `log into the
database <https://db.debian.org/login.html>`__ to change various
information about themselves, such as:

-  forwarding address for your debian.org email

-  subscription to debian-private

-  whether you are on vacation

-  personal information such as your address, country, the latitude and
   longitude of the place where you live for use in `the world map of
   Debian developers <https://www.debian.org/devel/developers.loc>`__,
   phone and fax numbers, IRC nickname and web page

-  password and preferred shell on Debian Project machines

Most of the information is not accessible to the public, naturally. For
more information please read the online documentation that you can find
at https://db.debian.org/doc-general.html\ .

Developers can also submit their SSH keys to be used for authorization
on the official Debian machines, and even add new \*.debian.net DNS
entries. Those features are documented at
https://db.debian.org/doc-mail.html\ .

.. _archive:

The Debian archive
================================================================================================================================

The Debian distribution consists of a lot of packages (currently around
|number-of-pkgs| source packages) and a few additional files (such
as documentation and installation disk images).

Here is an example directory tree of a complete Debian archive:

::

   dists/stable/main/
   dists/stable/main/binary-amd64/
   dists/stable/main/binary-armel/
   dists/stable/main/binary-i386/
        ...
   dists/stable/main/source/
        ...
   dists/stable/main/disks-amd64/
   dists/stable/main/disks-armel/
   dists/stable/main/disks-i386/
        ...

   dists/stable/contrib/
   dists/stable/contrib/binary-amd64/
   dists/stable/contrib/binary-armel/
   dists/stable/contrib/binary-i386/
        ...
   dists/stable/contrib/source/

   dists/stable/non-free/
   dists/stable/non-free/binary-amd64/
   dists/stable/non-free/binary-armel/
   dists/stable/non-free/binary-i386/
        ...
   dists/stable/non-free/source/

   dists/testing/
   dists/testing/main/
        ...
   dists/testing/contrib/
        ...
   dists/testing/non-free/
        ...

   dists/unstable
   dists/unstable/main/
        ...
   dists/unstable/contrib/
        ...
   dists/unstable/non-free/
        ...

   pool/
   pool/main/a/
   pool/main/a/apt/
        ...
   pool/main/b/
   pool/main/b/bash/
        ...
   pool/main/liba/
   pool/main/liba/libalias-perl/
        ...
   pool/main/m/
   pool/main/m/mailx/
        ...
   pool/non-free/f/
   pool/non-free/f/firmware-nonfree/
        ...

As you can see, the top-level directory contains two directories,
``dists/`` and ``pool/``. The latter is a “pool” in which the packages
actually are, and which is handled by the archive maintenance database
and the accompanying programs. The former contains the distributions,
``stable``, ``testing`` and ``unstable``. The ``Packages`` and
``Sources`` files in the distribution subdirectories can reference files
in the ``pool/`` directory. The directory tree below each of the
distributions is arranged in an identical manner. What we describe below
for ``stable`` is equally applicable to the ``unstable`` and ``testing``
distributions.

``dists/stable`` contains three directories, namely ``main``,
``contrib``, and ``non-free``.

In each of the areas, there is a directory for the source packages
(``source``) and a directory for each supported architecture
(``binary-i386``, ``binary-amd64``, etc.).

The ``main`` area contains additional directories which hold the disk
images and some essential pieces of documentation required for
installing the Debian distribution on a specific architecture
(``disks-i386``, ``disks-amd64``, etc.).

.. _archive-sections:

Sections
--------------------------------------------------------------------------------------------------------------------------------

The ``main`` section of the Debian archive is what makes up the
**official Debian distribution**. The ``main`` section is official
because it fully complies with all our guidelines. The other two
sections do not, to different degrees; as such, they are **not**
officially part of Debian.

Every package in the main section must fully comply with the `Debian
Free Software
Guidelines <https://www.debian.org/social_contract#guidelines>`__ (DFSG)
and with all other policy requirements as described in the `Debian
Policy Manual <https://www.debian.org/doc/debian-policy/>`__. The DFSG
is our definition of “free software.” Check out the Debian Policy Manual
for details.

Packages in the ``contrib`` section have to comply with the DFSG, but
may fail other requirements. For instance, they may depend on non-free
packages.

Packages which do not conform to the DFSG are placed in the ``non-free``
section. These packages are not considered as part of the Debian
distribution, though we enable their use, and we provide infrastructure
(such as our bug-tracking system and mailing lists) for non-free
software packages.

The `Debian Policy Manual <https://www.debian.org/doc/debian-policy/>`__
contains a more exact definition of the three sections. The above
discussion is just an introduction.

The separation of the three sections at the top-level of the archive is
important for all people who want to distribute Debian, either via FTP
servers on the Internet or on CD-ROMs: by distributing only the ``main``
and ``contrib`` sections, one can avoid any legal risks. Some packages
in the ``non-free`` section do not allow commercial distribution, for
example.

On the other hand, a CD-ROM vendor could easily check the individual
package licenses of the packages in ``non-free`` and include as many on
the CD-ROMs as it's allowed to. (Since this varies greatly from vendor
to vendor, this job can't be done by the Debian developers.)

Note that the term section is also used to refer to categories which
simplify the organization and browsing of available packages: ``admin``,
``net``, ``utils``, etc. Once upon a time, these sections (subsections,
rather) existed in the form of subdirectories within the Debian archive.
Nowadays, these exist only in the Section header fields of packages.

.. _s4.6.2:

Architectures
--------------------------------------------------------------------------------------------------------------------------------

In the first days, the Linux kernel was only available for Intel i386
(or greater) platforms, and so was Debian. But as Linux became more and
more popular, the kernel was ported to other architectures and Debian
started to support them. And as if supporting so much hardware was not
enough, Debian decided to build some ports based on other Unix kernels,
like ``hurd`` and ``kfreebsd``.

Debian GNU/Linux 1.3 was only available as ``i386``. Debian 2.0 shipped
for ``i386`` and ``m68k`` architectures. Debian 2.1 shipped for the
``i386``, ``m68k``, ``alpha``, and ``sparc`` architectures. Since then
Debian has grown hugely. Debian 9 supports a total of ten Linux
architectures (``amd64``, ``arm64``, ``armel``, ``armhf``, ``i386``,
``mips``, ``mips64el``, ``mipsel``, ``ppc64el``, and ``s390x``) and two
kFreeBSD architectures (``kfreebsd-i386`` and ``kfreebsd-amd64``).

Information for developers and users about the specific ports are
available at the `Debian Ports web
pages <https://www.debian.org/ports/>`__.

.. _s4.6.3:

Packages
--------------------------------------------------------------------------------------------------------------------------------

There are two types of Debian packages, namely ``source`` and ``binary``
packages.

Depending on the format of the source package, it will consist of one or
more files in addition to the mandatory ``.dsc`` file:

-  with format “1.0”, it has either a ``.tar.gz`` file or both an
   ``.orig.tar.gz`` and a ``.diff.gz`` file;

-  with format “3.0 (quilt)”, it has a mandatory
   ``.orig.tar.{gz,bz2,xz}`` upstream tarball, multiple optional
   ``.orig-``\ *component*\ ``.tar.{gz,bz2,xz}`` additional upstream
   tarballs and a mandatory ``debian.tar.{gz,bz2,xz}`` debian tarball;

-  with format “3.0 (native)”, it has only a single ``.tar.{gz,bz2,xz}``
   tarball.

If a package is developed specially for Debian and is not distributed
outside of Debian, there is just one ``.tar.{gz,bz2,xz}`` file, which
contains the sources of the program; it's called a “native” source
package. If a package is distributed elsewhere too, the
``.orig.tar.{gz,bz2,xz}`` file stores the so-called
``upstream source code``, that is the source code that's distributed by
the ``upstream maintainer`` (often the author of the software). In this
case, the ``.diff.gz`` or the ``debian.tar.{gz,bz2,xz}`` contains the
changes made by the Debian maintainer.

The ``.dsc`` file lists all the files in the source package together
with checksums (``md5sums``, ``sha1sums``, ``sha256sums``) and some
additional info about the package (maintainer, version, etc.).

.. _s4.6.4:

Distributions
--------------------------------------------------------------------------------------------------------------------------------

The directory system described in the previous chapter is itself
contained within ``distribution directories``. Each distribution is
actually contained in the ``pool`` directory in the top level of the
Debian archive itself.

To summarize, the Debian archive has a root directory within a mirror
site. For instance, at the mirror site ``ftp.us.debian.org`` the
Debian archive itself is contained in
`/debian <http://ftp.us.debian.org/debian>`__, which is a common location
(another is ``/pub/debian``).

A distribution comprises Debian source and binary packages, and the
respective ``Sources`` and ``Packages`` index files, containing the
header information from all those packages. The former are kept in the
``pool/`` directory, while the latter are kept in the ``dists/``
directory of the archive (for backwards compatibility).

.. _sec-dists:

Stable, testing, and unstable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are always distributions called ``stable`` (residing in
``dists/stable``), ``testing`` (residing in ``dists/testing``), and
``unstable`` (residing in ``dists/unstable``). This reflects the
development process of the Debian project.

Active development is done in the ``unstable`` distribution (that's why
this distribution is sometimes called the ``development
distribution``). Every Debian developer can update their packages in
this distribution at any time. Thus, the contents of this distribution
change from day to day. Since no special effort is made to make sure
everything in this distribution is working properly, it is sometimes
literally unstable.

The :ref:`testing` distribution is generated automatically by
taking packages from ``unstable`` if they satisfy certain criteria.
Those criteria should ensure a good quality for packages within
``testing``. The update to ``testing`` is launched twice each day, right
after the new packages have been installed. See :ref:`testing`.

After a period of development, once the release manager deems fit, the
``testing`` distribution is frozen, meaning that the policies which
control how packages move from ``unstable`` to ``testing`` are
tightened. Packages which are too buggy are removed. No changes are
allowed into ``testing`` except for bug fixes. After some time has
elapsed, depending on progress, the ``testing`` distribution is frozen
even further. Details of the handling of the testing distribution are
published by the Release Team on debian-devel-announce. After the open
issues are solved to the satisfaction of the Release Team, the
distribution is released. Releasing means that ``testing`` is renamed to
``stable``, and a new copy is created for the new ``testing``, and the
previous ``stable`` is renamed to ``oldstable`` and stays there until it
is finally archived. On archiving, the contents are moved to
``archive.debian.org``.

This development cycle is based on the assumption that the ``unstable``
distribution becomes ``stable`` after passing a period of being in
``testing``. Even once a distribution is considered stable, a few bugs
inevitably remain — that's why the stable distribution is updated every
now and then. However, these updates are tested very carefully and have
to be introduced into the archive individually to reduce the risk of
introducing new bugs. You can find proposed additions to ``stable`` in
the ``proposed-updates`` directory. Those packages in
``proposed-updates`` that pass muster are periodically moved as a batch
into the stable distribution and the revision level of the stable
distribution is incremented (e.g., ‘6.0’ becomes ‘6.0.1’, ‘5.0.7’
becomes ‘5.0.8’, and so forth). Please refer to :ref:`upload-stable` for details.

Note that development under ``unstable`` continues during the freeze
period, since the ``unstable`` distribution remains in place in parallel
with ``testing``.

.. _s4.6.4.2:

More information about the testing distribution
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Packages are usually installed into the ``testing`` distribution after
they have undergone some degree of testing in ``unstable``.

For more details, please see the :ref:`testing`.

Experimental
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``experimental`` distribution is a special distribution. It is not a
full distribution in the same sense as ``stable``, ``testing`` and
``unstable`` are. Instead, it is meant to be a temporary staging area
for highly experimental software where there's a good chance that the
software could break your system, or software that's just too unstable
even for the ``unstable`` distribution (but there is a reason to package
it nevertheless). Users who download and install packages from
``experimental`` are expected to have been duly warned. In short, all
bets are off for the ``experimental`` distribution.

These are the sources.list 5 lines for ``experimental``:

::

   deb http://deb.debian.org/debian/ experimental main
   deb-src http://deb.debian.org/debian/ experimental main

If there is a chance that the software could do grave damage to a
system, it is likely to be better to put it into ``experimental``. For
instance, an experimental compressed file system should probably go into
``experimental``.

Whenever there is a new upstream version of a package that introduces
new features but breaks a lot of old ones, it should either not be
uploaded, or be uploaded to ``experimental``. A new, beta, version of
some software which uses a completely different configuration can go
into ``experimental``, at the maintainer's discretion. If you are
working on an incompatible or complex upgrade situation, you can also
use ``experimental`` as a staging area, so that testers can get early
access.

Some experimental software can still go into ``unstable``, with a few
warnings in the description, but that isn't recommended because packages
from ``unstable`` are expected to propagate to ``testing`` and thus to
``stable``. You should not be afraid to use ``experimental`` since it
does not cause any pain to the ftpmasters, the experimental packages are
periodically removed once you upload the package in ``unstable`` with a
higher version number.

New software which isn't likely to damage your system can go directly
into ``unstable``.

An alternative to ``experimental`` is to use your personal web space on
``people.debian.org``.

.. _codenames:

Release code names
--------------------------------------------------------------------------------------------------------------------------------

Every released Debian distribution has a ``code name``: Debian
|version-oldoldstable| is called |codename-oldoldstable|;
Debian |version-oldstable|, |codename-oldstable|; Debian
|version-stable|, |codename-stable|; the next release,
Debian |version-testing|, will be called |codename-testing|
and Debian |version-nexttesting| will be called
|codename-nexttesting|. There is also a *pseudo-distribution*,
called ``sid``, which is the current ``unstable`` distribution; since
packages are moved from ``unstable`` to ``testing`` as they approach
stability, ``sid`` itself is never released. As well as the usual
contents of a Debian distribution, ``sid`` contains packages for
architectures which are not yet officially supported or released by
Debian. These architectures are planned to be integrated into the
mainstream distribution at some future date. The codenames and versions
for older releases are `listed <https://www.debian.org/releases/>`__ on
the website.

Since Debian has an open development model (i.e., everyone can
participate and follow the development) even the ``unstable`` and
``testing`` distributions are distributed to the Internet through the
Debian FTP and HTTP server network. Thus, if we had called the directory
which contains the release candidate version ``testing``, then we would
have to rename it to ``stable`` when the version is released, which
would cause all FTP mirrors to re-retrieve the whole distribution (which
is quite large).

On the other hand, if we called the distribution directories
``Debian-x.y`` from the beginning, people would think that Debian
release ``x.y`` is available. (This happened in the past, where a CD-ROM
vendor built a Debian 1.0 CD-ROM based on a pre-1.0 development version.
That's the reason why the first official Debian release was 1.1, and not
1.0.)

Thus, the names of the distribution directories in the archive are
determined by their code names and not their release status (e.g.,
|codename-stable|). These names stay the same during the
development period and after the release; symbolic links, which can be
changed easily, indicate the currently released stable distribution.
That's why the real distribution directories use the ``code names``,
while symbolic links for ``stable``, ``testing``, and ``unstable`` point
to the appropriate release directories.

.. _mirrors:

Debian mirrors
================================================================================================================================

The various download archives and the web site have several mirrors
available in order to relieve our canonical servers from heavy load. In
fact, some of the canonical servers aren't public — a first tier of
mirrors balances the load instead. That way, users always access the
mirrors and get used to using them, which allows Debian to better spread
its bandwidth requirements over several servers and networks, and
basically makes users avoid hammering on one primary location. Note that
the first tier of mirrors is as up-to-date as it can be since they
update when triggered from the internal sites (we call this push
mirroring).

All the information on Debian mirrors, including a list of the available
public FTP/HTTP servers, can be found at
https://www.debian.org/mirror/\ . This useful page also includes
information and tools which can be helpful if you are interested in
setting up your own mirror, either for internal or public access.

Note that mirrors are generally run by third parties who are interested
in helping Debian. As such, developers generally do not have accounts on
these machines.

.. _incoming-system:

The Incoming system
================================================================================================================================

The Incoming system is responsible for collecting updated packages and
installing them in the Debian archive. It consists of a set of
directories and scripts that are installed on ``ftp-master.debian.org``.

Packages are uploaded by all the maintainers into a directory called
``UploadQueue``. This directory is scanned every few minutes by a daemon
called ``queued``, ``*.command``-files are executed, and remaining and
correctly signed ``*.changes``-files are moved together with their
corresponding files to the ``unchecked`` directory. This directory is
not visible for most Developers, as ftp-master is restricted; it is
scanned every 15 minutes by the ``dak process-upload`` script, which
verifies the integrity of the uploaded packages and their cryptographic
signatures. If the package is considered ready to be installed, it is
moved into the ``done`` directory. If this is the first upload of the
package (or it has new binary packages), it is moved to the ``new``
directory, where it waits for approval by the ftpmasters. If the package
contains files to be installed by hand it is moved to the ``byhand``
directory, where it waits for manual installation by the ftpmasters.
Otherwise, if any error has been detected, the package is refused and is
moved to the ``reject`` directory.

Once the package is accepted, the system sends a confirmation mail to
the maintainer and closes all the bugs marked as fixed by the upload,
and the auto-builders may start recompiling it. The package is now
publicly accessible at https://incoming.debian.org/\  until it is
really installed in the Debian archive. This happens four times a day
(and is also called the ``dinstall run`` for historical reasons); the
package is then removed from incoming and installed in the pool along
with all the other packages. Once all the other updates (generating new
``Packages`` and ``Sources`` index files for example) have been made, a
special script is called to ask all the primary mirrors to update
themselves.

The archive maintenance software will also send the OpenPGP/GnuPG signed
``.changes`` file that you uploaded to the appropriate mailing lists. If
a package is released with the ``Distribution`` set to ``stable``, the
announcement is sent to ``debian-changes@lists.debian.org``. If a
package is released with ``Distribution`` set to ``unstable`` or
``experimental``, the announcement will be posted to
``debian-devel-changes@lists.debian.org`` or
``debian-experimental-changes@lists.debian.org`` instead.

Though ftp-master is restricted, a copy of the installation is available
to all developers on ``mirror.ftp-master.debian.org``.

.. _pkg-info:

Package information
================================================================================================================================

.. _pkg-info-web:

On the web
--------------------------------------------------------------------------------------------------------------------------------

Each package has several dedicated web pages.
``https://packages.debian.org/``\ *package-name* displays each version
of the package available in the various distributions. Each version
links to a page which provides information, including the package
description, the dependencies, and package download links.

The bug tracking system tracks bugs for each package. You can view the
bugs of a given package at the URL
``https://bugs.debian.org/``\ *package-name*.

.. _dak-ls:

The ``dak ls`` utility
--------------------------------------------------------------------------------------------------------------------------------

``dak ls`` is part of the dak suite of tools, listing available package
versions for all known distributions and architectures. The ``dak`` tool
is available on ``ftp-master.debian.org``, and on the mirror on
``mirror.ftp-master.debian.org``. It uses a single argument
corresponding to a package name. An example will explain it better:

::

   $ dak ls evince
   evince | 0.1.5-2sarge1 |     oldstable | source, alpha, arm, hppa, i386, ia64, m68k, mips, mipsel, powerpc, s390, sparc
   evince |    0.4.0-5 |     etch-m68k | source, m68k
   evince |    0.4.0-5 |        stable | source, alpha, amd64, arm, hppa, i386, ia64, mips, mipsel, powerpc, s390, sparc
   evince |   2.20.2-1 |       testing | source
   evince | 2.20.2-1+b1 |       testing | alpha, amd64, arm, armel, hppa, i386, ia64, mips, mipsel, powerpc, s390, sparc
   evince |   2.22.2-1 |      unstable | source, alpha, amd64, arm, armel, hppa, i386, ia64, m68k, mips, mipsel, powerpc, s390, sparc

In this example, you can see that the version in ``unstable`` differs
from the version in ``testing`` and that there has been a binary-only
NMU of the package for all architectures. Each version of the package
has been recompiled on all architectures.

.. _pkg-tracker:

The Debian Package Tracker
================================================================================================================================

The Debian Package Tracker is an email and web-based tool to track the
activity of a source package. You can get the same emails that the
package maintainer gets, simply by subscribing to the package in the
Debian Package Tracker.

The package tracker has a web interface at
https://tracker.debian.org/\  that puts together a lot of
information about each source package. It features many useful links
(BTS, QA stats, contact information, DDTP translation status, buildd
logs) and gathers much more information from various places (30 latest
changelog entries, testing status, etc.). It's a very useful tool if you
want to know what's going on with a specific source package.
Furthermore, once authenticated, you can subscribe and unsubscribe from
any package with a single click.

You can jump directly to the web page concerning a specific source
package with a URL like
``https://tracker.debian.org/pkg/``\ *sourcepackage*.

For more in-depth information, you should have a look at its
`documentation <https://qa.pages.debian.net/distro-tracker/>`__. Among
other things, it explains you how to interact with it by email, how to
filter the mails that it forwards, how to configure your VCS commit
notifications, how to leverage its features for maintainer teams, etc.

.. _ddpo:

Developer's packages overview
================================================================================================================================

A QA (quality assurance) web portal is available at
https://qa.debian.org/developer.php\  which displays a table
listing all the packages of a single developer (including those where
the party is listed as a co-maintainer). The table gives a good summary
about the developer's packages: number of bugs by severity, list of
available versions in each distribution, testing status and much more
including links to any other useful information.

It is a good idea to look up your own data regularly so that you don't
forget any open bugs, and so that you don't forget which packages are
your responsibility.

.. _alioth:

Debian's FusionForge installation: Alioth
================================================================================================================================

Until Alioth has been depreciated and eventually turned off in June
2018, it was a Debian service based on a slightly modified version of
the FusionForge software (which evolved from SourceForge and GForge).
This software offered developers access to easy-to-use tools such as bug
trackers, patch managers, project/task managers, file hosting services,
mailing lists, VCS repositories, etc.

For many previously offered services replacements exist. This is
important to know, as there are still many references to alioth which
still need fixing. If you encounter such references please take the time
to try fixing them, for example by filing bugs or when possible fixing
the reference.

.. _developer-misc:

Goodies for Debian Members
================================================================================================================================

Benefits available to Debian Members are documented on
https://wiki.debian.org/MemberBenefits\ .
