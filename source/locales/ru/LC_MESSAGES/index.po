
msgid ""
msgstr ""
"Project-Id-Version: developers-reference \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-12 11:37+0900\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: ../index.rst:2
msgid "Debian Developer's Reference"
msgstr ""

#: ../index.rst:4
msgid "Developer's Reference Team <developers-reference@packages.debian.org>"
msgstr ""

#: ../index.rst:6
msgid "Copyright © 2004, 2005, 2006, 2007 Andreas Barth"
msgstr ""

#: ../index.rst:7
msgid "Copyright © 1998, 1999, 2000, 2001, 2002, 2003 Adam Di Carlo"
msgstr ""

#: ../index.rst:8
msgid "Copyright © 2002, 2003, 2008, 2009 Raphaël Hertzog"
msgstr ""

#: ../index.rst:9
msgid "Copyright © 2008, 2009 Lucas Nussbaum"
msgstr ""

#: ../index.rst:10
msgid "Copyright © 1997, 1998 Christian Schwarz"
msgstr ""

#: ../index.rst:12
msgid ""
"This manual is free software; you may redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the "
"Free Software Foundation; either version 2, or (at your option) any later"
" version."
msgstr ""

#: ../index.rst:16
msgid ""
"This is distributed in the hope that it will be useful, but without any "
"warranty; without even the implied warranty of merchantability or fitness"
" for a particular purpose. See the GNU General Public License for more "
"details."
msgstr ""

#: ../index.rst:20
msgid ""
"A copy of the GNU General Public License is available as /usr/share"
"/common-licenses/GPL-2 in the Debian distribution or on the World Wide "
"Web at the GNU web site. You can also obtain it by writing to the Free "
"Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA "
"02110-1301, USA."
msgstr ""

#: ../index.rst:26
msgid ""
"This is Debian Developer's Reference version |VERSION|\\ , released on "
"|PUBDATE|\\ ."
msgstr ""

#: ../index.rst:29
msgid ""
"If you want to print this reference, you should use the pdf version. This"
" manual is also available in some other languages."
msgstr ""

#: ../index.rst:46
msgid "Appendix"
msgstr ""

