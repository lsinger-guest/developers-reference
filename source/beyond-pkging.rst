.. _beyond-pkging:

Beyond Packaging
********************************************************************************************************************************

Debian is about a lot more than just packaging software and maintaining
those packages. This chapter contains information about ways, often
really critical ways, to contribute to Debian beyond simply creating and
maintaining packages.

As a volunteer organization, Debian relies on the discretion of its
members in choosing what they want to work on and in choosing the most
critical thing to spend their time on.

.. _submit-bug:

Bug reporting
================================================================================================================================

We encourage you to file bugs as you find them in Debian packages. In
fact, Debian developers are often the first line testers. Finding and
reporting bugs in other developers' packages improves the quality of
Debian.

Read the `instructions for reporting
bugs <https://www.debian.org/Bugs/Reporting>`__ in the Debian `bug
tracking system <https://www.debian.org/Bugs/>`__.

Try to submit the bug from a normal user account at which you are likely
to receive mail, so that people can reach you if they need further
information about the bug. Do not submit bugs as root.

You can use a tool like reportbug 1 to submit bugs. It can automate and
generally ease the process.

Make sure the bug is not already filed against a package. Each package
has a bug list easily reachable at
``https://bugs.debian.org/``\ *packagename*. Utilities like querybts 1
can also provide you with this information (and ``reportbug`` will
usually invoke ``querybts`` before sending, too).

Try to direct your bugs to the proper location. When for example your
bug is about a package which overwrites files from another package,
check the bug lists for *both* of those packages in order to avoid
filing duplicate bug reports.

For extra credit, you can go through other packages, merging bugs which
are reported more than once, or tagging bugs ``fixed`` when they have
already been fixed. Note that when you are neither the bug submitter nor
the package maintainer, you should not actually close the bug (unless
you secure permission from the maintainer).

From time to time you may want to check what has been going on with the
bug reports that you submitted. Take this opportunity to close those
that you can't reproduce anymore. To find out all the bugs you
submitted, you just have to visit
``https://bugs.debian.org/from:``\ *your-email-addr*.

.. _submit-many-bugs:

Reporting lots of bugs at once (mass bug filing)
--------------------------------------------------------------------------------------------------------------------------------

Reporting a great number of bugs for the same problem on a great number
of different packages — i.e., more than 10 — is a deprecated practice.
Take all possible steps to avoid submitting bulk bugs at all. For
instance, if checking for the problem can be automated, add a new check
to ``lintian`` so that an error or warning is emitted.

If you report more than 10 bugs on the same topic at once, it is
recommended that you send a message to ``debian-devel@lists.debian.org``
describing your intention before submitting the report, and mentioning
the fact in the subject of your mail. This will allow other developers
to verify that the bug is a real problem. In addition, it will help
prevent a situation in which several maintainers start filing the same
bug report simultaneously.

Please use the programs ``dd-list`` and if appropriate ``whodepends``
(from the package ``devscripts``) to generate a list of all affected
packages, and include the output in your mail to
``debian-devel@lists.debian.org``.

Note that when sending lots of bugs on the same subject, you should send
the bug report to ``maintonly@bugs.debian.org`` so that the bug report
is not forwarded to the bug distribution mailing list.

The program ``mass-bug`` (from the package ``devscripts``) can be used
to file bug reports against a list of packages.

Usertags
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You may wish to use BTS usertags when submitting bugs across a number of
packages. Usertags are similar to normal tags such as 'patch' and
'wishlist' but differ in that they are user-defined and occupy a
namespace that is unique to a particular user. This allows multiple sets
of developers to 'usertag' the same bug in different ways without
conflicting.

To add usertags when filing bugs, specify the ``User`` and ``Usertags``
pseudo-headers:

::

   To: submit@bugs.debian.org
   Subject: title-of-bug

   Package: pkgname
   [ ... ]
   User: email-addr
   Usertags: tag-name [ tag-name ... ]

   description-of-bug ...

Note that tags are separated by spaces and cannot contain underscores.
If you are filing bugs for a particular group or team it is recommended
that you set the ``User`` to an appropriate mailing list after
describing your intention there.

To view bugs tagged with a specific usertag, visit
``https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=``\ *email-addr*\ ``&tag=``\ *tag-name*.

.. _qa-effort:

Quality Assurance effort
================================================================================================================================

.. _qa-daily-work:

Daily work
--------------------------------------------------------------------------------------------------------------------------------

Even though there is a dedicated group of people for Quality Assurance,
QA duties are not reserved solely for them. You can participate in this
effort by keeping your packages as bug-free as possible, and as
lintian-clean (see :ref:`lintian`) as possible. If you do not find
that possible, then you should consider orphaning some of your packages
(see :ref:`orphaning`). Alternatively, you may ask the help of other
people in order to catch up with the backlog of bugs that you have (you
can ask for help on ``debian-qa@lists.debian.org`` or
``debian-devel@lists.debian.org``). At the same time, you can look for
co-maintainers (see :ref:`collaborative-maint`).

.. _qa-bsp:

Bug squashing parties
--------------------------------------------------------------------------------------------------------------------------------

From time to time the QA group organizes bug squashing parties to get
rid of as many problems as possible. They are announced on
``debian-devel-announce@lists.debian.org`` and the announcement explains
which area will be the focus of the party: usually they focus on release
critical bugs but it may happen that they decide to help finish a major
upgrade (like a new ``perl`` version that requires recompilation of all
the binary modules).

The rules for non-maintainer uploads differ during the parties because
the announcement of the party is considered prior notice for NMU. If you
have packages that may be affected by the party (because they have
release critical bugs for example), you should send an update to each of
the corresponding bug to explain their current status and what you
expect from the party. If you don't want an NMU, or if you're only
interested in a patch, or if you will deal with the bug yourself, please
explain that in the BTS.

People participating in the party have special rules for NMU; they can
NMU without prior notice if they upload their NMU to DELAYED/3-day at
least. All other NMU rules apply as usual; they should send the patch of
the NMU to the BTS (to one of the open bugs fixed by the NMU, or to a
new bug, tagged fixed). They should also respect any particular wishes
of the maintainer.

If you don't feel confident about doing an NMU, just send a patch to the
BTS. It's far better than a broken NMU.

.. _contacting-maintainers:

Contacting other maintainers
================================================================================================================================

During your lifetime within Debian, you will have to contact other
maintainers for various reasons. You may want to discuss a new way of
cooperating between a set of related packages, or you may simply remind
someone that a new upstream version is available and that you need it.

Looking up the email address of the maintainer for the package can be
distracting. Fortunately, there is a simple email alias,
*package*\ ``@packages.debian.org``, which provides a way to email the
maintainer, whatever their individual email address (or addresses) may
be. Replace *package* with the name of a source or a binary package.

You may also be interested in contacting the persons who are subscribed
to a given source package via :ref:`pkg-tracker`. You can do so by
using the *package*\ ``@packages.qa.debian.org`` email address.

.. _mia-qa:

Dealing with inactive and/or unreachable maintainers
================================================================================================================================

If you notice that a package is lacking maintenance, you should make
sure that the maintainer is active and will continue to work on their
packages. It is possible that they are not active anymore, but haven't
registered out of the system, so to speak. On the other hand, it is also
possible that they just need a reminder.

There is a simple system (the MIA database) in which information about
maintainers who are deemed Missing In Action is recorded. When a member
of the QA group contacts an inactive maintainer or finds more
information about one, this is recorded in the MIA database. This system
is available in ``/org/qa.debian.org/mia`` on the host
``qa.debian.org``, and can be queried with the ``mia-query`` tool. Use
``mia-query --help`` to see how to query the database. If you find that
no information has been recorded about an inactive maintainer yet, or
that you can add more information, you should generally proceed as
follows.

The first step is to politely contact the maintainer, and wait a
reasonable time for a response. It is quite hard to define reasonable
time, but it is important to take into account that real life is
sometimes very hectic. One way to handle this would be to send a
reminder after two weeks.

A non-functional e-mail address is a `violation of Debian
Policy <https://www.debian.org/doc/debian-policy/#the-maintainer-of-a-package>`__.
If an e-mail "bounces", please file a bug against the package and submit
this information to the MIA database.

If the maintainer doesn't reply within four weeks (a month), one can
assume that a response will probably not happen. If that happens, you
should investigate further, and try to gather as much useful information
about the maintainer in question as possible. This includes:

-  The ``echelon`` information available through the `developers' LDAP
   database <https://db.debian.org/>`__, which indicates when the
   developer last posted to a Debian mailing list. (This includes mails
   about uploads distributed via the
   ``debian-devel-changes@lists.debian.org`` list.) Also, remember to
   check whether the maintainer is marked as on vacation in the
   database.

-  The number of packages this maintainer is responsible for, and the
   condition of those packages. In particular, are there any RC bugs
   that have been open for ages? Furthermore, how many bugs are there in
   general? Another important piece of information is whether the
   packages have been NMUed, and if so, by whom.

-  Is there any activity of the maintainer outside of Debian? For
   example, they might have posted something recently to non-Debian
   mailing lists or news groups.

A bit of a problem are packages which were sponsored — the maintainer is
not an official Debian developer. The ``echelon`` information is not
available for sponsored people, for example, so you need to find and
contact the Debian developer who has actually uploaded the package.
Given that they signed the package, they're responsible for the upload
anyhow, and are likely to know what happened to the person they
sponsored.

It is also allowed to post a query to ``debian-devel@lists.debian.org``,
asking if anyone is aware of the whereabouts of the missing maintainer.
Please Cc: the person in question.

Once you have gathered all of this, you can contact
``mia@qa.debian.org``. People on this alias will use the information you
provide in order to decide how to proceed. For example, they might
orphan one or all of the packages of the maintainer. If a package has
been NMUed, they might prefer to contact the NMUer before orphaning the
package — perhaps the person who has done the NMU is interested in the
package.

One last word: please remember to be polite. We are all volunteers and
cannot dedicate all of our time to Debian. Also, you are not aware of
the circumstances of the person who is involved. Perhaps they might be
seriously ill or might even have died — you do not know who may be on
the receiving side. Imagine how a relative will feel if they read the
e-mail of the deceased and find a very impolite, angry and accusing
message!

On the other hand, although we are volunteers, a package maintainer has
made a commitment and therefore has a responsibility to maintain the
package. So you can stress the importance of the greater good — if a
maintainer does not have the time or interest anymore, they should let
go and give the package to someone with more time and/or interest.

If you are interested in working on the MIA team, please have a look at
the ``README`` file in ``/org/qa.debian.org/mia`` on ``qa.debian.org``,
where the technical details and the MIA procedures are documented, and
contact ``mia@qa.debian.org``.

.. _newmaint:

Interacting with prospective Debian developers
================================================================================================================================

Debian's success depends on its ability to attract and retain new and
talented volunteers. If you are an experienced developer, we recommend
that you get involved with the process of bringing in new developers.
This section describes how to help new prospective developers.

.. _sponsoring:

Sponsoring packages
--------------------------------------------------------------------------------------------------------------------------------

Sponsoring a package means uploading a package for a maintainer who is
not able to do it on their own. It's not a trivial matter; the sponsor
must verify the packaging and ensure that it is of the high level of
quality that Debian strives to have.

Debian Developers can sponsor packages. Debian Maintainers can't.

The process of sponsoring a package is:

1. The maintainer prepares a source package (``.dsc``) and puts it
   online somewhere (like on
   `mentors.debian.net <https://mentors.debian.net/cgi-bin/welcome>`__)
   or even better, provides a link to a public VCS repository (see
   :ref:`salsa-debian-org`) where the package is maintained.

2. The sponsor downloads (or checks out) the source package.

3. The sponsor reviews the source package. If they find issues, they
   inform the maintainer and ask them to provide a fixed version (the
   process starts over at step 1).

4. The sponsor could not find any remaining problem. They build the
   package, sign it, and upload it to Debian.

Before delving into the details of how to sponsor a package, you should
ask yourself whether adding the proposed package is beneficial to
Debian.

There's no simple rule to answer this question; it can depend on many
factors: is the upstream codebase mature and not full of security holes?
Are there pre-existing packages that can do the same task and how do
they compare to this new package? Has the new package been requested by
users and how large is the user base? How active are the upstream
developers?

You should also ensure that the prospective maintainer is going to be a
good maintainer. Do they already have some experience with other
packages? If yes, are they doing a good job with them (check out some
bugs)? Are they familiar with the package and its programming language?
Do they have the skills needed for this package? If not, are they able
to learn them?

It's also a good idea to know where they stand with respect to Debian:
do they agree with Debian's philosophy and do they intend to join
Debian? Given how easy it is to become a Debian Member, you might want
to only sponsor people who plan to join. That way you know from the
start that you won't have to act as a sponsor indefinitely.

.. _sponsoring-new-package:

Sponsoring a new package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

New maintainers usually have certain difficulties creating Debian
packages — this is quite understandable. They will make mistakes. That's
why sponsoring a brand new package into Debian requires a thorough
review of the Debian packaging. Sometimes several iterations will be
needed until the package is good enough to be uploaded to Debian. Thus
being a sponsor implies being a mentor.

Don't ever sponsor a new package without reviewing it. The review of new
packages done by ftpmasters mainly ensures that the software is really
free. Of course, it happens that they stumble on packaging problems but
they really should not. It's your task to ensure that the uploaded
package complies with the Debian Free Software Guidelines and is of good
quality.

Building the package and testing the software is part of the review, but
it's also not enough. The rest of this section contains a non-exhaustive
list of points to check in your review.  [1]_

-  Verify that the upstream tarball provided is the same that has been
   distributed by the upstream author (when the sources are repackaged
   for Debian, generate the modified tarball yourself).

-  Run ``lintian`` (see :ref:`lintian`). It will catch many common
   problems. Be sure to verify that any ``lintian`` overrides set up by
   the maintainer are fully justified.

-  Run ``licensecheck`` (part of :ref:`devscripts`) and verify that
   ``debian/copyright`` seems correct and complete. Look for license
   problems (like files with “All rights reserved” headers, or with a
   non-DFSG compliant license). ``grep -ri`` is your friend for this
   task.

-  Build the package with ``pbuilder`` (or any similar tool, see
   :ref:`pbuilder`) to ensure that the build-dependencies are
   complete.

-  Proofread ``debian/control``: does it follow the best practices (see
   :ref:`bpp-debian-control`)? Are the dependencies complete?

-  Proofread ``debian/rules``: does it follow the best practices (see
   :ref:`bpp-debian-rules`)? Do you see some possible improvements?

-  Proofread the maintainer scripts (``preinst``, ``postinst``,
   ``prerm``, ``postrm``, ``config``): will the ``preinst``/``postrm``
   work when the dependencies are not installed? Are all the scripts
   idempotent (i.e. can you run them multiple times without
   consequences)?

-  Review any change to upstream files (either in ``.diff.gz``, or in
   ``debian/patches/`` or directly embedded in the ``debian`` tarball
   for binary files). Are they justified? Are they properly documented
   (with `DEP-3 <https://dep-team.pages.debian.net/deps/dep3/>`__ for
   patches)?

-  For every file, ask yourself why the file is there and whether it's
   the right way to achieve the desired result. Is the maintainer
   following the best packaging practices (see
   :ref:`best-pkging-practices`)?

-  Build the packages, install them and try the software. Ensure that
   you can remove and purge the packages. Maybe test them with
   ``piuparts``.

If the audit did not reveal any problems, you can build the package and
upload it to Debian. Remember that even if you're not the maintainer, as
a sponsor you are still responsible for what you upload to Debian.
That's why you're encouraged to keep up with the package through
:ref:`pkg-tracker`.

Note that you should not need to modify the source package to put your
name in the ``changelog`` or in the ``control`` file. The ``Maintainer``
field of the ``control`` file and the ``changelog`` should list the
person who did the packaging, i.e. the sponsee. That way they will get
all the BTS mail.

Instead, you should instruct ``dpkg-buildpackage`` to use your key for
the signature. You do that with the ``-k`` option:

::

   dpkg-buildpackage -kKEY-ID

If you use ``debuild`` and ``debsign``, you can even configure it
permanently in ``~/.devscripts``:

::

   DEBSIGN_KEYID=KEY-ID

.. _sponsoring-update:

Sponsoring an update of an existing package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You will usually assume that the package has already gone through a full
review. So instead of doing it again, you will carefully analyze the
difference between the current version and the new version prepared by
the maintainer. If you have not done the initial review yourself, you
might still want to have a deeper look just in case the initial reviewer
was sloppy.

To be able to analyze the difference, you need both versions. Download
the current version of the source package (with ``apt-get source``) and
rebuild it (or download the current binary packages with
``aptitude download``). Download the source package to sponsor (usually
with ``dget``).

Read the new changelog entry; it should tell you what to expect during
the review. The main tool you will use is ``debdiff`` (provided by the
``devscripts`` package); you can run it with two source packages
(``.dsc`` files), or two binary packages, or two ``.changes`` files (it
will then compare all the binary packages listed in the ``.changes``).

If you compare the source packages (excluding upstream files in the case
of a new upstream version, for example by filtering the output of
``debdiff`` with ``filterdiff -i '*/debian/*'``), you must understand
all the changes you see and they should be properly documented in the
Debian changelog.

If everything is fine, build the package and compare the binary packages
to verify that the changes on the source package have no unexpected
consequences (some files dropped by mistake, missing dependencies,
etc.).

You might want to check out the Package Tracking System (see
:ref:`pkg-tracker`) to verify if the maintainer has not missed
something important. Maybe there are translation updates sitting in the
BTS that could have been integrated. Maybe the package has been NMUed
and the maintainer forgot to integrate the changes from the NMU into
their package. Maybe there's a release critical bug that they have left
unhandled and that's blocking migration to ``testing``. If you find
something that they could have done (better), it's time to tell them so
that they can improve for next time, and so that they have a better
understanding of their responsibilities.

If you have found no major problem, upload the new version. Otherwise
ask the maintainer to provide you a fixed version.

Advocating new developers
--------------------------------------------------------------------------------------------------------------------------------

See the page about `advocating a prospective
developer <https://www.debian.org/devel/join/nm-advocate>`__ at the
Debian web site.

.. _become-application-manager:

Handling new maintainer applications
--------------------------------------------------------------------------------------------------------------------------------

Please see `Checklist for Application
Managers <https://www.debian.org/devel/join/nm-amchecklist>`__ at the
Debian web site.

.. [1]
   You can find more checks in the wiki, where several developers share
   their own `sponsorship
   checklists <https://wiki.debian.org/SponsorChecklist>`__.
